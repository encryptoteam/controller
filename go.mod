module gitlab.com/encryptoteam/rocket-apps/services/controller

go 1.16

// replace gitlab.com/encryptoteam/rocket-apps/services/proto => /Users/dev/projects/gitlab.com/encryptoteam/rocket-apps/services/proto
// replace github.com/sergey-shpilevskiy/go-cert => /Users/dev/projects/github.com/sergey-shpilevskiy/go-cert

require (
	github.com/bykovme/goconfig v0.0.0-20170717154220-caa70d3abfca
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.11.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/lib/pq v1.10.4
	github.com/mattn/go-sqlite3 v1.14.15
	github.com/sergey-shpilevskiy/go-cert v0.0.3
	gitlab.com/encryptoteam/rocket-apps/services/proto v0.7.1
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	golang.org/x/sys v0.0.0-20210915083310-ed5796bab164 // indirect
	google.golang.org/grpc v1.50.1
	google.golang.org/protobuf v1.27.1
)
