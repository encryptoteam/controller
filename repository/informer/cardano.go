package informer

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"
	"time"

	cardanoPB "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/cardano"
	"gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/common"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	grpcStatus "google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/model"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/auth"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/helpers"
)

type DBNodeStatistic interface {
	GetNodeByID(uuid string) (model.Node, error)
	GetLastParamData(uuid, block, param string) (paramData model.NodeStatistic, err error)

	InsertNodeStatistic(data model.NodeStatistic) error
	UpdateNode(data model.Node) (err error)
}

// CardanoInformServer -
type CardanoInformServer struct {
	NodeStatistics map[string]NodeStatistics
	loadedConfig   config.Config
	jwtManager     *auth.JWTManager
	db             DBNodeStatistic

	cardanoPB.UnimplementedCardanoServer
}

// NewCardanoInformServer -
func NewCardanoInformServer(jwtManager *auth.JWTManager, loadedConfig config.Config, db DBNodeStatistic) *CardanoInformServer {
	return &CardanoInformServer{
		NodeStatistics: make(map[string]NodeStatistics),
		jwtManager:     jwtManager,
		loadedConfig:   loadedConfig,
		db:             db,
	}
}

// SaveStatistic -
func (server *CardanoInformServer) SaveStatistic(ctx context.Context, request *cardanoPB.SaveStatisticRequest) (response *cardanoPB.SaveStatisticResponse, err error) {
	// Check if node is in config
	if !helpers.Contains(server.loadedConfig.Nodes, request.NodeAuthData.Uuid) {
		// TODO: Add more inormation in response
		response = &cardanoPB.SaveStatisticResponse{
			Status: "Error",
		}
		return response, nil
	}

	// Get node from memory
	nodeStatistic, exist := server.NodeStatistics[request.NodeAuthData.Uuid]
	if !exist {
		nodeStatistic = NodeStatistics{
			NodeAuthData: request.NodeAuthData,
			Statistic:    new(cardanoPB.StatisticResponse),
		}
	} else {
		if request.NodeAuthData != nil {
			nodeStatistic.NodeAuthData = request.NodeAuthData
		}
	}

	nodeStatistic.CheckRequestData(request.Statistic)

	server.NodeStatistics[request.NodeAuthData.Uuid] = nodeStatistic

	dataNodes := model.Node{
		NodeAuthData: *nodeStatistic.NodeAuthData,
	}
	err = server.db.UpdateNode(dataNodes)
	if err != nil {
		log.Println(fmt.Errorf("error update node: %w. Data: %v", err, dataNodes))
		return response, err
	}
	// nodeStatistic.SaveNodeData(server.db, request.NodeAuthData.Uuid)

	response = &cardanoPB.SaveStatisticResponse{
		Status: "Ok",
	}

	return response, nil
}

// GetStatistic -
func (server *CardanoInformServer) GetStatistic(ctx context.Context, request *cardanoPB.GetStatisticRequest) (response *cardanoPB.GetStatisticResponse, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return response, grpcStatus.Errorf(codes.Unauthenticated, "metadata is not provided")
	}

	values := md["authorization"]
	if len(values) == 0 {
		return response, grpcStatus.Errorf(codes.Unauthenticated, "authorization token is not provided")
	}

	claims, err := server.jwtManager.Verify(values[0])
	if err != nil {
		return response, grpcStatus.Errorf(codes.Unauthenticated, "access token is invalid: %v", err)
	}

	response = new(cardanoPB.GetStatisticResponse)
	response.Statistic = new(cardanoPB.StatisticResponse)

	node, exist := server.NodeStatistics[request.Uuid]
	if !exist {
		// Get data from DB
		nodeDtata, err := server.db.GetNodeByID(request.Uuid)
		if err != nil {
			return response, grpcStatus.Errorf(codes.Internal, "failed to get data from DB: %v", err)
		}
		node.NodeAuthData = &nodeDtata.NodeAuthData

		node.GetNodeData(server.db, request.Uuid)
		node.CheckSelfData()
	}

	response.NodeAuthData = node.NodeAuthData
	// response.Statistic = node.Statistic

	for _, permission := range claims.Permissions {
		switch permission {
		case "basic":
			response.Statistic.NodeBasicData = node.Statistic.NodeBasicData
			response.Statistic.ServerBasicData = node.Statistic.ServerBasicData
			response.Statistic.Online = node.Statistic.Online

		case "server_technical":
			response.Statistic.MemoryState = node.Statistic.MemoryState
			response.Statistic.CpuState = node.Statistic.CpuState
			response.Statistic.Updates = node.Statistic.Updates
			response.Statistic.Security = node.Statistic.Security

		case "node_technical":
			response.Statistic.Epoch = node.Statistic.Epoch
			response.Statistic.NodeState = node.Statistic.NodeState
			response.Statistic.NodePerformance = node.Statistic.NodePerformance
			response.Statistic.KesData = node.Statistic.KesData

		case "node_financial":
			response.Statistic.Blocks = node.Statistic.Blocks
			response.Statistic.StakeInfo = node.Statistic.StakeInfo
		}
	}

	return response, nil
}

func (n *NodeStatistics) GetNodeData(db DBNodeStatistic, uuid string) {
	errs := make([]error, 0)
	defer func() {
		for _, v := range errs {
			log.Println(v)
		}
	}()

	statisticResponse := cardanoPB.StatisticResponse{}
	statisticBlocks := reflect.ValueOf(&statisticResponse).Elem()
	numField := statisticBlocks.NumField()
	for i := 0; i < numField; i++ { // iterate over all fields in struct StatisticResponse
		// Get name of field
		blockName := statisticBlocks.Type().Field(i).Name
		if blockName == "state" || blockName == "sizeCache" || blockName == "unknownFields" {
			continue
		}
		blockField := statisticBlocks.Field(i)

		// Check if field is pointer
		if blockField.Kind() == reflect.Ptr {
			// Create new pointer
			blockField.Set(reflect.New(blockField.Type().Elem()))
		}

		data := blockField.Elem().FieldByName("Data")
		// Check if field is pointer
		if data.Kind() == reflect.Ptr {
			// Create new pointer
			data.Set(reflect.New(data.Type().Elem()))
		}

		data = data.Elem()
		dataNumField := data.NumField()
		for f := 0; f < dataNumField; f++ { // iterate over all fields in struct Data
			paramName := data.Type().Field(f).Name
			if paramName == "state" || paramName == "sizeCache" || paramName == "unknownFields" {
				continue
			}

			paramType := data.Type().Field(f).Type

			paramData, err := db.GetLastParamData(uuid, blockName, paramName)
			if err != nil {
				errs = append(errs, err)
				// log.Println(err)
				continue
			}

			value, err := convertToType(paramData.Value, paramType)
			if err != nil {
				// log.Println(err)
				errs = append(errs, err)
				continue
			}

			data.Field(f).Set(reflect.ValueOf(value))
		}

		commonStatus := &common.Status{
			LastUpdate: timestamppb.New(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
		}

		statusData, err := db.GetLastParamData(uuid, blockName, "LastUpdate")
		if err != nil {
			// log.Println(err)
			errs = append(errs, err)
		} else {
			commonStatus.LastUpdate = timestamppb.New(statusData.LastUpdate)
		}

		sd := blockField.Elem().FieldByName("Status")
		sd.Set(reflect.ValueOf(commonStatus))
	}

	n.Statistic = &statisticResponse
}

func (n *NodeStatistics) SaveNodeData(db DBNodeStatistic, uuid string) {
	errs := make([]error, 0)
	defer func() {
		for _, v := range errs {
			log.Println(v)
		}
	}()

	statisticBlocks := reflect.ValueOf(n.Statistic).Elem()
	for i := 0; i < statisticBlocks.NumField(); i++ { // iterate over all fields in struct StatisticResponse
		// Get name of field
		blockName := statisticBlocks.Type().Field(i).Name
		if blockName == "state" || blockName == "sizeCache" || blockName == "unknownFields" {
			continue
		}
		blockField := statisticBlocks.Field(i)
		blockType := statisticBlocks.Type().Field(i).Type

		data := blockField.Elem().FieldByName("Data")
		if data.IsNil() {
			errs = append(errs, fmt.Errorf("block %v is nil", blockName))
			continue
		}

		data = data.Elem()

		isBlockchain := false
		if !strings.Contains(blockType.String(), "common.") {
			isBlockchain = true
		}

		for f := 0; f < data.NumField(); f++ { // iterate over all fields in struct Data
			paramName := data.Type().Field(f).Name
			if paramName == "state" || paramName == "sizeCache" || paramName == "unknownFields" {
				continue
			}

			paramValue := data.Field(f).Interface()

			nodeStatistic := model.NodeStatistic{
				UUID:       uuid,
				Block:      blockName,
				Param:      paramName,
				Value:      fmt.Sprintf("%v", paramValue),
				Blockchain: isBlockchain,
			}

			err := db.InsertNodeStatistic(nodeStatistic)
			if err != nil {
				errs = append(errs, err)
				// log.Println(err)
				continue
			}
		}

		nodeStatistic := model.NodeStatistic{
			UUID:       uuid,
			Block:      blockName,
			Param:      "LastUpdate",
			Value:      time.Now().String(),
			Blockchain: isBlockchain,
		}

		err := db.InsertNodeStatistic(nodeStatistic)
		if err != nil {
			errs = append(errs, err)
			// log.Println(err)
			continue
		}
	}
}

func convertToType(v interface{}, t reflect.Type) (interface{}, error) {
	switch t.Kind() {
	case reflect.Int64:
		return strconv.ParseInt(v.(string), 10, 64)
	case reflect.Bool:
		return strconv.ParseBool(v.(string))
	case reflect.Uint64:
		return strconv.ParseUint(v.(string), 10, 64)
	case reflect.Float32:
		f, err := strconv.ParseFloat(v.(string), 32)
		if err != nil {
			return nil, err
		}
		return float32(f), nil
	case reflect.Float64:
		return strconv.ParseFloat(v.(string), 64)
	default:
		return v.(string), nil
	}
}
