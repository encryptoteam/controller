package informer

import (
	"reflect"
	"sync"

	cardanoPB "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/cardano"
	commonPB "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/common"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type NodeStatistics struct {
	NodeAuthData *commonPB.NodeAuthData
	Statistic    *cardanoPB.StatisticResponse
}

func (n *NodeStatistics) CheckRequestData(statistic *cardanoPB.StatisticRequest) {
	lastUpdate := timestamppb.Now()

	var wg sync.WaitGroup
	wg.Add(13)
	go n.checkNodeBasicData(&wg, statistic.NodeBasicData, lastUpdate)
	go n.checkServerBasicData(&wg, statistic.ServerBasicData, lastUpdate)
	go n.checkEpoch(&wg, statistic.Epoch, lastUpdate)
	go n.checkKesData(&wg, statistic.KesData, lastUpdate)
	go n.checkBlocks(&wg, statistic.Blocks, lastUpdate)
	go n.checkUpdates(&wg, statistic.Updates, lastUpdate)
	go n.checkSecurity(&wg, statistic.Security, lastUpdate)
	go n.checkStakeInfo(&wg, statistic.StakeInfo, lastUpdate)
	go n.checkOnline(&wg, statistic.Online, lastUpdate)
	go n.checkMemoryState(&wg, statistic.MemoryState, lastUpdate)
	go n.checkCPUState(&wg, statistic.CpuState, lastUpdate)
	go n.checkNodeState(&wg, statistic.NodeState, lastUpdate)
	go n.checkNodePerformance(&wg, statistic.NodePerformance, lastUpdate)
	wg.Wait()
}

func (n *NodeStatistics) CheckSelfData() {
	var wg sync.WaitGroup
	wg.Add(13)
	go n.checkNodeBasicData(&wg, n.Statistic.NodeBasicData.Data, n.Statistic.NodeBasicData.Status.LastUpdate)
	go n.checkServerBasicData(&wg, n.Statistic.ServerBasicData.Data, n.Statistic.ServerBasicData.Status.LastUpdate)
	go n.checkEpoch(&wg, n.Statistic.Epoch.Data, n.Statistic.Epoch.Status.LastUpdate)
	go n.checkKesData(&wg, n.Statistic.KesData.Data, n.Statistic.KesData.Status.LastUpdate)
	go n.checkBlocks(&wg, n.Statistic.Blocks.Data, n.Statistic.Blocks.Status.LastUpdate)
	go n.checkUpdates(&wg, n.Statistic.Updates.Data, n.Statistic.Updates.Status.LastUpdate)
	go n.checkSecurity(&wg, n.Statistic.Security.Data, n.Statistic.Security.Status.LastUpdate)
	go n.checkStakeInfo(&wg, n.Statistic.StakeInfo.Data, n.Statistic.StakeInfo.Status.LastUpdate)
	go n.checkOnline(&wg, n.Statistic.Online.Data, n.Statistic.Online.Status.LastUpdate)
	go n.checkMemoryState(&wg, n.Statistic.MemoryState.Data, n.Statistic.MemoryState.Status.LastUpdate)
	go n.checkCPUState(&wg, n.Statistic.CpuState.Data, n.Statistic.CpuState.Status.LastUpdate)
	go n.checkNodeState(&wg, n.Statistic.NodeState.Data, n.Statistic.NodeState.Status.LastUpdate)
	go n.checkNodePerformance(&wg, n.Statistic.NodePerformance.Data, n.Statistic.NodePerformance.Status.LastUpdate)
	wg.Wait()
}

func (n *NodeStatistics) checkNodeBasicData(wg *sync.WaitGroup, nodeBasicData *commonPB.NodeBasicData, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()

	if nodeBasicData != nil && !reflect.DeepEqual(nodeBasicData, &commonPB.NodeBasicData{}) {
		n.Statistic.NodeBasicData = &commonPB.NodeBasicDataResponse{
			Data: nodeBasicData,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.NodeBasicData = &commonPB.NodeBasicDataResponse{
			Data: nodeBasicData,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10001",
						ErrorMessage: "Node basic data is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkServerBasicData(wg *sync.WaitGroup, serverBasicData *commonPB.ServerBasicData, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if serverBasicData != nil && !reflect.DeepEqual(serverBasicData, &commonPB.ServerBasicData{}) {
		n.Statistic.ServerBasicData = &commonPB.ServerBasicDataResponse{
			Data: serverBasicData,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.ServerBasicData = &commonPB.ServerBasicDataResponse{
			Data: serverBasicData,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10002",
						ErrorMessage: "ServerBasicData is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkEpoch(wg *sync.WaitGroup, epoch *cardanoPB.Epoch, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if epoch != nil && !reflect.DeepEqual(epoch, &cardanoPB.Epoch{}) {
		n.Statistic.Epoch = &cardanoPB.EpochResponse{
			Data: epoch,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.Epoch = &cardanoPB.EpochResponse{
			Data: epoch,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10003",
						ErrorMessage: "Epoch is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkKesData(wg *sync.WaitGroup, kesData *cardanoPB.KESData, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if kesData != nil && !reflect.DeepEqual(kesData, &cardanoPB.KESData{}) {
		n.Statistic.KesData = &cardanoPB.KESDataResponse{
			Data: kesData,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.KesData = &cardanoPB.KESDataResponse{
			Data: kesData,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10004",
						ErrorMessage: "KesData is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkBlocks(wg *sync.WaitGroup, blocks *cardanoPB.Blocks, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if blocks != nil && !reflect.DeepEqual(blocks, &cardanoPB.Blocks{}) {
		n.Statistic.Blocks = &cardanoPB.BlocksResponse{
			Data: blocks,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.Blocks = &cardanoPB.BlocksResponse{
			Data: blocks,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10005",
						ErrorMessage: "Blocks is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkUpdates(wg *sync.WaitGroup, updates *commonPB.Updates, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if updates != nil && !reflect.DeepEqual(updates, &commonPB.Updates{}) {
		n.Statistic.Updates = &commonPB.UpdatesResponse{
			Data: updates,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.Updates = &commonPB.UpdatesResponse{
			Data: updates,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10006",
						ErrorMessage: "Updates is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkSecurity(wg *sync.WaitGroup, security *commonPB.Security, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if security != nil && !reflect.DeepEqual(security, &commonPB.Security{}) {
		n.Statistic.Security = &commonPB.SecurityResponse{
			Data: security,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.Security = &commonPB.SecurityResponse{
			Data: security,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10007",
						ErrorMessage: "Security is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkStakeInfo(wg *sync.WaitGroup, stakeInfo *cardanoPB.StakeInfo, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if stakeInfo != nil && !reflect.DeepEqual(stakeInfo, &cardanoPB.StakeInfo{}) {
		n.Statistic.StakeInfo = &cardanoPB.StakeInfoResponse{
			Data: stakeInfo,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.StakeInfo = &cardanoPB.StakeInfoResponse{
			Data: stakeInfo,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10008",
						ErrorMessage: "StakeInfo is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkOnline(wg *sync.WaitGroup, online *commonPB.Online, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if online != nil && !reflect.DeepEqual(online, &commonPB.Online{}) {
		n.Statistic.Online = &commonPB.OnlineResponse{
			Data: online,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.Online = &commonPB.OnlineResponse{
			Data: online,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10009",
						ErrorMessage: "Online is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkMemoryState(wg *sync.WaitGroup, memoryState *commonPB.MemoryState, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if memoryState != nil && !reflect.DeepEqual(memoryState, &commonPB.MemoryState{}) {
		n.Statistic.MemoryState = &commonPB.MemoryStateResponse{
			Data: memoryState,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.MemoryState = &commonPB.MemoryStateResponse{
			Data: memoryState,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10010",
						ErrorMessage: "MemoryState is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkCPUState(wg *sync.WaitGroup, cpuState *commonPB.CPUState, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if cpuState != nil && !reflect.DeepEqual(cpuState, &commonPB.CPUState{}) {
		n.Statistic.CpuState = &commonPB.CPUStateResponse{
			Data: cpuState,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.CpuState = &commonPB.CPUStateResponse{
			Data: cpuState,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10011",
						ErrorMessage: "CpuState is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkNodeState(wg *sync.WaitGroup, nodeState *cardanoPB.NodeState, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if nodeState != nil && !reflect.DeepEqual(nodeState, &cardanoPB.NodeState{}) {
		n.Statistic.NodeState = &cardanoPB.NodeStateResponse{
			Data: nodeState,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.NodeState = &cardanoPB.NodeStateResponse{
			Data: nodeState,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10012",
						ErrorMessage: "NodeState is empty",
					},
				},
			},
		}
	}
}

func (n *NodeStatistics) checkNodePerformance(wg *sync.WaitGroup, nodePerformance *cardanoPB.NodePerformance, lastUpdate *timestamppb.Timestamp) {
	defer wg.Done()
	if nodePerformance != nil && !reflect.DeepEqual(nodePerformance, &cardanoPB.NodePerformance{}) {
		n.Statistic.NodePerformance = &cardanoPB.NodePerformanceResponse{
			Data: nodePerformance,
			Status: &commonPB.Status{
				Status:     "ok",
				LastUpdate: lastUpdate,
			},
		}
	} else {
		n.Statistic.NodePerformance = &cardanoPB.NodePerformanceResponse{
			Data: nodePerformance,
			Status: &commonPB.Status{
				Status:     "error",
				LastUpdate: lastUpdate,
				Errors: []*commonPB.Error{
					{
						ErrorCode:    "10013",
						ErrorMessage: "NodePerformance is empty",
					},
				},
			},
		}
	}
}
