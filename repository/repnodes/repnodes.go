package repnodes

import (
	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/model"
)

type DBNodeStatistic interface {
	GetNodeByID(uuid string) (model.Node, error)
}

type RepoNodes struct {
	db DBNodeStatistic
}

func InitController(db DBNodeStatistic) RepoNodes {
	return RepoNodes{db: db}
}

func (c *RepoNodes) GetNodeByID(uuid string) (model.Node, error) {
	return c.db.GetNodeByID(uuid)
}
