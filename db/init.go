package db

import (
	"database/sql"
	"log"

	"github.com/go-playground/validator/v10"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/mem"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/model"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/mysql"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/postgresql"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/sqlDB"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/sqlite3"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
)

type DB interface {
	GetNodeByID(uuid string) (model.Node, error)
	GetLastParamData(uuid, block, param string) (paramData model.NodeStatistic, err error)

	InsertNodeStatistic(data model.NodeStatistic) error
	UpdateNode(data model.Node) (err error)

	Ping() error
	Close() error
}

func InitDB(config config.DBConfig) (DB, error) {
	log.Println("Initing DB")
	defer log.Println("DB inited")

	var db *sql.DB
	var err error
	validate := validator.New()

	switch config.DBType {
	case "postgres":
		err = validate.Struct(config.Postgres)
		if err != nil {
			return nil, err
		}

		db, err = postgresql.OpenDB(config.Postgres)
	case "mysql":
		err = validate.Struct(config.MySQL)
		if err != nil {
			return nil, err
		}

		db, err = mysql.OpenDB(config.MySQL)
	case "sqlite3":
		err = validate.Struct(config.SQLite3)
		if err != nil {
			return nil, err
		}

		db, err = sqlite3.OpenDB(config.SQLite3)
	default:
		return mem.InitDB()
	}
	if err != nil {
		return nil, err
	}

	c, err := sqlDB.InitSQL(db)
	if err != nil {
		log.Println(err)
		return c, err
	}

	return c, err
}
