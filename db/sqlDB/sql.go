package sqlDB

import (
	"database/sql"
	"log"
)

type Connection struct {
	db *sql.DB
}

func InitSQL(db *sql.DB) (c *Connection, err error) {
	c = &Connection{db: db}

	if err = c.createTables(); err != nil {
		log.Println(err)
		return c, err
	}

	return c, err
}

func (c *Connection) Ping() error {
	return c.db.Ping()
}

func (c *Connection) Close() error {
	return c.db.Close()
}
