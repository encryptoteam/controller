package mysql

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
)

func OpenDB(config config.MySQLConfig) (db *sql.DB, err error) {
	log.Println("Opening mysql connection...")

	db, err = sql.Open("mysql", config.User+":"+config.Password+"@tcp("+config.Host+":"+config.Port+")/"+config.DBname)
	if err != nil {
		return db, err
	}

	if err = db.Ping(); err != nil {
		return db, err
	}

	return db, nil
}
