package mem

import (
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/model"
)

type DB interface {
	GetNodeByID(uuid string) (model.Node, error)
	GetLastParamData(uuid, block, param string) (paramData model.NodeStatistic, err error)

	InsertNodeStatistic(data model.NodeStatistic) error
	UpdateNode(data model.Node) (err error)

	Ping() error
	Close() error
}

type node struct {
	data model.Node
}

type nodeStatisticsKey struct {
	UUID  string
	Block string
	Param string
}

type Mem struct {
	nodes          map[string]node
	nodeStatistics map[nodeStatisticsKey]model.NodeStatistic
}

func InitDB() (DB, error) {
	log.Println("Initing Mem DB")
	return &Mem{
		nodes:          make(map[string]node),
		nodeStatistics: make(map[nodeStatisticsKey]model.NodeStatistic),
	}, nil
}

// +++
func (m *Mem) GetNodeByID(uuid string) (model.Node, error) {
	node, ok := m.nodes[uuid]
	if !ok {
		return model.Node{}, errors.New("node not found")
	}

	return node.data, nil
}

// +++
func (m *Mem) GetLastParamData(uuid, block, param string) (paramData model.NodeStatistic, err error) {
	paramData, ok := m.nodeStatistics[nodeStatisticsKey{UUID: uuid, Block: block, Param: param}]
	if !ok {
		return paramData, fmt.Errorf("UUID: %s, Block: %s, Param: %s not found", uuid, block, param)
	}

	return paramData, nil
}

// +++
func (m *Mem) InsertNodeStatistic(data model.NodeStatistic) error {
	data.LastUpdate = time.Now()
	m.nodeStatistics[nodeStatisticsKey{UUID: data.UUID, Block: data.Block, Param: data.Param}] = data
	return nil
}

// +++
func (m *Mem) UpdateNode(data model.Node) (err error) {
	data.LastUpdate = time.Now()
	m.nodes[data.Uuid] = node{data: data}
	return nil
}

// +++
func (m *Mem) Ping() error {
	if m.nodes == nil {
		return errors.New("nodes is nil")
	}
	if m.nodeStatistics == nil {
		return errors.New("nodeStatistics is nil")
	}

	return nil
}

// +++
func (m *Mem) Close() error {
	m = nil
	return nil
}

// https://github.com/hashicorp/go-memdb
