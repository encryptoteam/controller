package sqlite3

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
)

func OpenDB(config config.SQLiteConfig) (db *sql.DB, err error) {
	log.Println("Opening sqlite connection...")
	defer func() {
		if err != nil {
			log.Println("Error opening sqlite connection: ", err)
		} else {
			log.Println("Sqlite connection opened")
		}
	}()

	db, err = sql.Open("sqlite3", config.Path+config.DBname)
	if err != nil {
		return db, err
	}

	if err = db.Ping(); err != nil {
		return db, err
	}

	return db, nil
}
